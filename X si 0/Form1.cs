﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace X_si_0
{
    public partial class Form1 : Form
    {
        bool tura = true;//true=tura x,flase=tura 0
        int tura_count = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void eXITToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (tura)

                b.Text = "X";
            else
                b.Text = "0";

            tura = !tura;
            b.Enabled = false;
            tura_count++;


            cineiicastigator();
            

        }
        private void cineiicastigator()

        {
            bool castigator = false;
            //orizontal
            if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!C1.Enabled))
                castigator = true;
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
                castigator = true;
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
                castigator = true;
            //vertical
            else if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
                castigator = true;
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B2.Enabled))
                castigator = true;
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C2.Enabled))
                castigator = true;
            //diagonala
            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!C3.Enabled))
                castigator = true;
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!A3.Enabled))
                castigator = true;

            

            if (castigator)
            {
                dezactivambutoane();

                string Castigator = "";
                if (tura)
                    Castigator = "0";
                else
                    Castigator = "X";


                MessageBox.Show(Castigator + " " + "Castiga");
            }
            else
            {

                if (tura_count == 9)
                    MessageBox.Show("Egalitate");

            }

            
        }


        private void jOCNOUToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tura = true;
            tura_count = 0;
            try
            {

                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";

                }
            }
            catch { }


        }
        private void dezactivambutoane()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;


                }


            }
            catch { }

        }
    }
}
